using FishNet.Broadcast;

namespace Authentication
{
    public struct PasswordBroadcast : IBroadcast
    {
        public string Password;
    }

    public struct ResponseBroadcast : IBroadcast
    {
        public bool Passed;
    }
}