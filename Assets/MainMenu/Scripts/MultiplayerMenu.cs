using System.Collections;
using System.Collections.Generic;
using Authentication;
using FishNet;
using UnityEngine;
using UnityEngine.UI;

public sealed class MultiplayerMenu : MonoBehaviour
{
    [SerializeField] private Button _hostButton;
    [SerializeField] private Button _connectButton;
    
    void Start()
    {
        _hostButton.onClick.AddListener(() =>
        {
            InstanceFinder.ServerManager.StartConnection();
            //InstanceFinder.ClientManager.StartConnection();

            PasswordBroadcast passwordBroadcast = new PasswordBroadcast();
            passwordBroadcast.Password = "HelloWorld";
            InstanceFinder.ClientManager.Broadcast(passwordBroadcast);

        });
        
        _connectButton.onClick.AddListener((() =>
        {
            InstanceFinder.ClientManager.StartConnection();
            
            PasswordBroadcast passwordBroadcast = new PasswordBroadcast();
            passwordBroadcast.Password = "HelloWorld";
            InstanceFinder.ClientManager.Broadcast(passwordBroadcast);
        }));
    }
    
}
